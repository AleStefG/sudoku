#include <stdlib.h>
#include <stdio.h>
#include "sudoku.h"
#include "list.h"
#include "mylib.h"

int isValid(int starti, int startj, int row, int column, int dim_prob, int** matrix){
	int i,j;
	
	for(i = 0 ; i < dim_prob * dim_prob; i++){
		if(i != column && matrix[row][i] != 0 && matrix[row][i] == matrix[row][column]){
		 	return 1;
		}
	}
	
	for(i = 0 ; i < dim_prob * dim_prob ; i++){
		if(i != row && matrix[i][column] != 0 && matrix[i][column] == matrix[row][column]){
		 	return 1;
		}
	}

	for(i = starti ; i < starti + dim_prob ; i++){
		for(j = startj ; j < startj + dim_prob ; j++){
			if((i != row || j != column) && matrix[i][j] != 0 
				&& matrix[i][j] == matrix[row][column]){
				return 1;
			}
		}
	}

	return 0;
}

void computeSudoku(int starti, int startj, int row, int column, int stop, int dim_prob, int** matrix, struct Node *results){
	int i;

	if(stop >= dim_prob*dim_prob){
		addLast(results, matrix, dim_prob*dim_prob);
		return;
	}

	if(matrix[row][column] == 0){
		for(i = 1 ; i <= dim_prob*dim_prob ; i++){
			matrix[row][column] = i;
			if(isValid(starti, startj, row, column, dim_prob, matrix) == 0){
				if(column == startj + dim_prob - 1){
					computeSudoku(starti, startj, row+1, startj, stop+1, dim_prob, matrix, results);
				} else {				
					computeSudoku(starti, startj, row, column+1, stop+1,  dim_prob, matrix, results);
				}
			}
		}
		matrix[row][column] = 0;

	} else {
		if(column == startj + dim_prob - 1){
			computeSudoku(starti, startj, row+1, startj, stop+1, dim_prob, matrix, results);
		} else {
			computeSudoku(starti, startj, row, column+1, stop+1, dim_prob, matrix, results);
		}
	}
	return;
} 