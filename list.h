#ifndef _LIST_H_
#define _LIST_H_

struct Mat{
	int **array;
	int size;
};

struct Node{
	struct Node *next;
	struct Mat *grid;
	int size;
};

struct Node* createList(void);
void addLast(struct Node *list, int **grid, int size);
void add_array_List(struct Node *list, int *grid, int size);
void printList(struct Node *list);
int** getValue(struct Node *list, int position);

#endif /*_LIST_H_*/