#ifndef _MYLIB_H_
#define _MYLIB_H_

void resolve(int starti, int startj, int dim_prob, int* matrix, struct Node *list);
int** makeMatrix(int* array, int size);
int* makeArray(int** matrix, int size);
int isOk(int* vec, int n);
int isReadyToSend(int **matrix, int dim_prob, int nr_squares);

#endif /* _MYLIB_h_ */