#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "mylib.h"

/**
*	Gheorghe Alexandru-Stefan
*/

struct Node *createList(void){
	struct Node *list = malloc(sizeof(struct Node));
	list->grid = malloc(sizeof(struct Mat));	
	list->next = NULL;
	list->size = 0;
	return list;
}

void addLast(struct Node *list, int **grid, int size){
	struct Node *iterator = list;
	int i, j;

	while (iterator->next != NULL) {
		iterator = iterator->next;
	}

	iterator->next = malloc(sizeof(struct Node));
	iterator->next->grid = malloc(sizeof(struct Mat));
	iterator->next->grid->array = malloc(size * sizeof(int*));
	for(i = 0 ; i < size ; i++){
		iterator->next->grid->array[i] = malloc(size * sizeof(int));
		for(j = 0 ; j < size ; j++){
			iterator->next->grid->array[i][j] = grid[i][j];
		}
	}
	iterator->next->grid->size = size;

	iterator->next->next = NULL;
	list->size++;
}

void add_array_List(struct Node *list, int *grid, int size){
	
	int **sudoku = makeMatrix(grid, size);
	addLast(list, sudoku, size);
}

void printList(struct Node *list){
	struct Node *iterator = list->next;
	int i, j, size;

	while (iterator != NULL) {
		size = iterator->grid->size;
		for(i = 0 ; i < size ; i++){
			for(j = 0 ; j < size ; j++){
				printf("%d ", iterator->grid->array[i][j]);
			}
			printf("\n");
		}
		printf("\n");
		iterator = iterator->next;
	}
}

int** getValue(struct Node *list, int position){
	struct Node *iterator = list;

	while (position > 0) {
		iterator = iterator->next;
		position--;
	}
	return iterator->grid->array;
}