#include "list.h"
#ifndef _SUDOKU_H_
#define _SUDOKU_H_

int isValid(int starti, int startj, int row, int column, int dim_prob, int** matrix);
void computeSudoku(int starti, int startj, int row, int column, int stop, int dim_prob, int** matrix, struct Node *results);

#endif /* _SUDOKU_H_*/