#include <stdio.h>
#include <stdlib.h>
#include "sudoku.h"
#include "list.h"

int** makeMatrix(int* array, int size){
	int i,j;
	int **sudoku = malloc(size*sizeof(int*));

	for(i = 0 ; i < size ; i++){
		sudoku[i] = malloc(size*sizeof(int));
		for(j = 0 ; j < size ; j++){
			sudoku[i][j] = array[i*size + j];
		}
	}
	return sudoku; 
}

int* makeArray(int** matrix, int size){
	int i,j;
	int *array = malloc(size*size*sizeof(int));

	for(i = 0 ; i < size ; i++){
		for(j = 0 ; j < size ; j++){
			array[i*size+j] = matrix[i][j];
		}
	}

	return array; 
}

void resolve(int starti, int startj, int dim_prob, int* matrix, struct Node *list){
	int **sudoku = makeMatrix(matrix, dim_prob*dim_prob);
	computeSudoku(starti, startj, starti, startj, 0, dim_prob, sudoku, list);
}

int isReadyToSend(int **matrix, int dim_prob, int nr_squares){
	int i, j, k, l;
	int ok = 0;
	for(i = 0 ; i < dim_prob*dim_prob ; i = i + dim_prob){
		for(j = 0 ; j < dim_prob*dim_prob ; j = j + dim_prob){
			for(k = i ; k < i + dim_prob ; k++){
				for(l = j ; l < j + dim_prob ; l++){
					if(matrix[k][l] == 0){
						ok = 1;
						break;
					}
				}
				if(ok == 1){
					break;				
				}
			}
			if(ok == 0){
				nr_squares--;
			}
			ok = 0;
		}
	}

	if(nr_squares == 0){
		return 1;
	} else {
		return 0;
	}
}

int isOk(int* vec, int n){
	int** matrix = makeMatrix(vec, n);
	int i, j, k;
	for(i = 0 ; i < n ; i++){
		for(j = 0 ; j < n ; j++){
			for(k = 0 ; k < n ; k++){
				if(i != k && j != k && matrix[i][j] != 0 && (matrix[i][j] == matrix[i][k] || matrix[i][j] == matrix[k][j]) ){
					return 0;			
				}
			}
		}
	}	
	return 1;
}