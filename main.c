// Distributed Sudoku
// Gheorghe Alexandru-Stefan
//***********************************//

#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "sudoku.h"
#include "mylib.h"

#define SONDA 			1
#define ECOU 			2
#define TOP_NULL 		3
#define SUDOKU 			4
#define SUDOKU_SOL 		5
#define FINAL			6

int main(int argc, char **argv) {

    int nr_vecini = 0, nr_ecouri = 0, nr_copii = 0, in_copii = 0;
	int numtasks, rank, sursa, tag, flag = 0, parinte, dim_prob, dim_mat, aux;  
	int inmsg, outmsg, nr_squares , last_free_square;
	int i, j, k, src;
	int *vecini, *in_vecini, *nul, *copii_ramura, *inS, *outS;
	char *line, *token;
	int **sudoku, **compute;
	struct Node *list = createList();
	int my_square, first_free_square, dim_square;
	int starti, startj, dim_list;
	int GATA;
	char *string, *auxString;
	FILE *in, *in2, *out;

	MPI_Status Stat;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

	if(argc < 3 ){
		printf("Folosire corecta: topologie.in sudoku.in sudoku.out \n");
		return -1;
	}

	vecini = (int*)malloc(numtasks*sizeof(int));
	in_vecini = (int*)malloc(numtasks*sizeof(int));
	nul = (int*)malloc(numtasks*sizeof(int));
	line = (char*)malloc(numtasks*2 + 3);
	copii_ramura = (int*)malloc(numtasks*sizeof(int));
	GATA = 0;

	memset(copii_ramura, 0, numtasks*sizeof(int));
	memset(line, 0, numtasks*2 + 3);

	in = fopen(argv[1], "r");
 	
	for(i = 0 ; i <= rank ; i++)
		fgets ( line, numtasks*2 + 3, in );

	fclose(in);	

	for(i = 0 ; i < numtasks ; i++){
		vecini[i] = -1;
		nul[i] = -1;
		for(j = 0 ; j < numtasks ; j++){	
		}
	}
	
	token = strtok(line, " -");
	if (token != NULL)
		token = strtok (NULL, " -");

	while (token != NULL){
		vecini[atoi(token)] = atoi(token);
		nr_vecini++;
    	token = strtok (NULL, " -");
  	}

	if (rank == 0) {
		for(i = 0 ; i < numtasks ; i++)
			if(vecini[i] != -1) // Daca e vecin(copil)	
				MPI_Send(nul, numtasks, MPI_INT, i, SONDA, MPI_COMM_WORLD);
		
		nr_ecouri = nr_vecini;

		while(nr_ecouri > 0){
			MPI_Recv(in_vecini, numtasks, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &Stat);
			tag = Stat.MPI_TAG;
			sursa = Stat.MPI_SOURCE;

			if(tag == SONDA){
				MPI_Send(nul, numtasks, MPI_INT, sursa, TOP_NULL, MPI_COMM_WORLD);
				if(vecini[sursa] == sursa)
					vecini[sursa] = -1;					
			} else if(tag == ECOU){

				nr_ecouri--;

			for(i = 0 ; i < numtasks ; i++)
					if(in_vecini[i] != rank){
						nr_copii++;
						in_copii++;
					}
					if(vecini[i] == -1 && in_vecini[i] != rank)
						vecini[i] = sursa;                       
				}
				copii_ramura[sursa] = in_copii;
				in_copii = 0;
			}
		}
		vecini[rank] = rank;
	} else {
		MPI_Recv(in_vecini, numtasks, MPI_INT, MPI_ANY_SOURCE, SONDA, MPI_COMM_WORLD, &Stat);
		parinte = Stat.MPI_SOURCE;

		if(nr_vecini == 1){
			for(i = 0 ; i < numtasks ; i++)
				vecini[i] = parinte;
			
			vecini[rank] = rank;
			MPI_Send(vecini, numtasks, MPI_INT, parinte, ECOU, MPI_COMM_WORLD);

		} else {
			for(i = 0 ; i < numtasks ; i++)
				if(vecini[i] != -1 && i != parinte)
					MPI_Send(nul, numtasks, MPI_INT, i, SONDA, MPI_COMM_WORLD);

			nr_ecouri = nr_vecini - 1;

			while(nr_ecouri > 0){
				MPI_Recv(in_vecini, numtasks, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &Stat);
				sursa = Stat.MPI_SOURCE;
				tag = Stat.MPI_TAG; 
				if(tag == SONDA){
					MPI_Send(nul, numtasks, MPI_INT, sursa, TOP_NULL, MPI_COMM_WORLD);
					if(vecini[sursa] == sursa)
						vecini[sursa] = -1;					
					
				} else if(tag == ECOU){

					nr_ecouri--;

					for(i = 0 ; i < numtasks ; i++){
						if(in_vecini[i] != rank){
							nr_copii++;
							in_copii++;
						}
						if(vecini[i] == -1 && in_vecini[i] != rank){
							vecini[i] = sursa;
						}
					}
					copii_ramura[sursa] = in_copii;
					in_copii = 0;
				} else if(tag == TOP_NULL){
					nr_ecouri--;
					if(vecini[sursa] == sursa){
						vecini[sursa] = -1;
					}
				}
			}

			for(i = 0 ; i < numtasks ; i++)
				if(vecini[i] == -1)
					vecini[i] = parinte;				
			
			vecini[rank] = rank;
			MPI_Send(vecini, numtasks, MPI_INT, parinte, ECOU, MPI_COMM_WORLD);			
		} 
	}
	string = (char*)malloc(100*sizeof(char));
	auxString = (char*)malloc(4*sizeof(char));

	sprintf(string, "%d:", rank);
	for(i = 0 ; i< numtasks; i++){
		sprintf(auxString, " %d", vecini[i]);
		strcat(string, auxString);
	}
	free(string);
	free(auxString);

//**************** FINAL TOPOLOGIE **********************//

	MPI_Barrier(MPI_COMM_WORLD);
	free(nul);
	free(line);

	in2 = fopen(argv[2], "r");
	fscanf( in2, "%d", &dim_prob);
	dim_mat = dim_prob*dim_prob;
	dim_square = (dim_mat + 1) * dim_mat;

	inS = (int*)malloc(dim_square * sizeof(int));
	outS = (int*)malloc(dim_square * sizeof(int));

	if(rank == 0){	

		for(i = 0 ; i < dim_mat  ; i++){
			for(j = 0 ; j < dim_mat  ; j++){
				fscanf( in2, "%d", &aux);
				outS[i*dim_mat + j] = aux;	
			}
		}
		fclose(in2);
		
		my_square = 0;
		first_free_square = 1;

		for(i = 0 ; i < numtasks ; i++){
			if(vecini[i] != rank && vecini[i] == i){
				outS[dim_mat*dim_mat] = first_free_square;
				outS[dim_mat*dim_mat + 1] = first_free_square + copii_ramura[i] - 1;
				first_free_square += copii_ramura[i];
				MPI_Send(outS, dim_square, MPI_INT, i, SUDOKU, MPI_COMM_WORLD);
			}
		}

	resolve(0,0,dim_prob, outS, list);

		while(1){
			for(src = 0 ; src < numtasks ; src++){
				if(vecini[src] != rank && vecini[src] == src){
					MPI_Iprobe(src, SUDOKU_SOL, MPI_COMM_WORLD, &flag, &Stat);
					if(flag){
						MPI_Recv(inS, dim_square, MPI_INT, src, SUDOKU_SOL, MPI_COMM_WORLD, &Stat);
						sursa = Stat.MPI_SOURCE;

						add_array_List(list, inS, dim_mat);
						sudoku = makeMatrix(inS, dim_mat);
						dim_list = list->size;

						for(i = 1 ; i < dim_list ; i++){
							compute = getValue(list, i);
							for(j = 0 ; j < dim_mat ; j++)
								for(k = 0 ; k < dim_mat ; k++)
									if(sudoku[j][k] == 0 && compute[j][k] != 0)
										sudoku[j][k] = compute[j][k];

							inS = makeArray(sudoku, dim_mat);
							if(isOk(inS, dim_mat)){

								if(isReadyToSend(sudoku, dim_prob, dim_mat)){
									GATA = 1;
									break;
								}
								addLast(list, sudoku, dim_mat);
							}
						}

						if(GATA == 1)
							break;									
					}
				}
			}
			if(GATA == 1)
				break;
		}
		
		for(i = 0 ; i < numtasks ; i++)
			if(vecini[i] != rank && vecini[i] == i) // Daca e vecin(copil) 
				MPI_Send(&outmsg, 1, MPI_INT, i, FINAL, MPI_COMM_WORLD);

		MPI_Iprobe(MPI_ANY_SOURCE, SUDOKU_SOL, MPI_COMM_WORLD, &flag, &Stat);
		while(flag){
			MPI_Recv(inS, dim_square, MPI_INT, MPI_ANY_SOURCE, SUDOKU_SOL, MPI_COMM_WORLD, &Stat);
			MPI_Iprobe(MPI_ANY_SOURCE, SUDOKU_SOL, MPI_COMM_WORLD, &flag, &Stat);
		}

		out = fopen(argv[3], "w");
		fprintf(out, "%d\n", dim_prob);
		for(i = 0 ; i < dim_mat ; i++){
			for(j = 0 ; j < dim_mat ; j++){
				fprintf(out, "%d ", sudoku[i][j]);
			}
			fprintf(out, "\n");
		}  
		
	} else {
		MPI_Recv(inS, dim_square, MPI_INT, MPI_ANY_SOURCE, SUDOKU, MPI_COMM_WORLD, &Stat);
		my_square = inS[dim_mat * dim_mat];
		startj = (my_square%dim_prob) * dim_prob;
		starti = (my_square/dim_prob)* dim_prob;

		first_free_square = my_square + 1;
		last_free_square = inS[dim_mat * dim_mat + 1];
		nr_squares = last_free_square - my_square + 1;

		if(nr_copii == 0){
			resolve(starti, startj, dim_prob, inS, list);

			for(i = 1 ; i <= list->size ; i++){
				compute = getValue(list, i);				
				inS = makeArray(compute, dim_mat);	
				MPI_Send(inS, dim_square, MPI_INT, parinte, SUDOKU_SOL, MPI_COMM_WORLD);

			}
			MPI_Recv(&inmsg, 1, MPI_INT, parinte, FINAL, MPI_COMM_WORLD, &Stat); 
		} else {
			for(i = 0 ; i < numtasks ; i++){
				if(vecini[i] != rank && vecini[i] == i && i != parinte){
					inS[dim_mat*dim_mat] = first_free_square;
					inS[dim_mat*dim_mat + 1] = first_free_square + copii_ramura[i] - 1;
					first_free_square += copii_ramura[i];

					MPI_Send(inS, dim_square, MPI_INT, i, SUDOKU, MPI_COMM_WORLD);
				}
			}
			resolve(starti,startj,dim_prob, inS, list);

			while(1){
				for(src = 0 ; src < numtasks ; src++){
					MPI_Iprobe(parinte, FINAL, MPI_COMM_WORLD, &flag, &Stat);
					if(flag){
						MPI_Recv(&inmsg, 1, MPI_INT, parinte, FINAL, MPI_COMM_WORLD, &Stat);
						GATA = 1;
						break;
					}
					if(vecini[src] != rank && vecini[src] == src){
						MPI_Iprobe(src, SUDOKU_SOL, MPI_COMM_WORLD, &flag, &Stat);
						if(flag){
							MPI_Recv(inS, dim_square, MPI_INT, src, SUDOKU_SOL, MPI_COMM_WORLD, &Stat);
							sursa = Stat.MPI_SOURCE;

							add_array_List(list, inS, dim_mat);
							sudoku = makeMatrix(inS, dim_mat);
							dim_list = list->size;
							for(i = 1 ; i < dim_list ; i++){
								compute = getValue(list, i);
								for(j = 0 ; j < dim_mat ; j++)
									for(k = 0 ; k < dim_mat ; k++)
										if(sudoku[j][k] == 0)
											sudoku[j][k] = compute[j][k];
								
								outS = makeArray(sudoku, dim_mat);
								if(isOk(outS, dim_mat)){
									if(isReadyToSend(sudoku, dim_prob, nr_squares)){
										MPI_Send(outS, dim_square, MPI_INT, parinte, SUDOKU_SOL, MPI_COMM_WORLD);				
									} else {
										addLast(list, sudoku, dim_mat);
									}
								}
							}							
						}
					}
				}
				if(GATA == 1)
					break;
			}

			for(i = 0 ; i < numtasks ; i++)
				if(vecini[i] != rank && vecini[i] == i && i != parinte)
					MPI_Send(&outmsg, 1, MPI_INT, i, FINAL, MPI_COMM_WORLD);

			MPI_Iprobe(MPI_ANY_SOURCE, SUDOKU_SOL, MPI_COMM_WORLD, &flag, &Stat);
			while(flag){
				MPI_Recv(inS, dim_square, MPI_INT, MPI_ANY_SOURCE, SUDOKU_SOL, MPI_COMM_WORLD, &Stat);
				MPI_Iprobe(MPI_ANY_SOURCE, SUDOKU_SOL, MPI_COMM_WORLD, &flag, &Stat);
			} 
		}
	} 
	free(vecini);
	free(in_vecini);
	free(copii_ramura);
	free(inS);
	free(outS);
	MPI_Finalize();
    return 0;
}